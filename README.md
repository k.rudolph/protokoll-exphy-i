## Einführung um Git in VisualStudio code zu verwenden.

Bevor man Git Repositories in VisualStudio code einbinden kann muss Git auf deinem Gerät installiert sein, siehe Internet wo und wie das geht.

Um diese Git Repository in mit VisualStudio code zu verlinken musst du in VisualStudio code (ab jetzt nur noch VScode) auf 'File' > 'Close Folder' gehen, dann anstatt auf 'open Folder' auf 'clone repository' gehen und den link https://gitlab.gwdg.de/k.rudolph/protokoll-exphy-i (findest du auch auf der Gitlab Seite des Projektes) eingeben, alle sonstigen angefragten Daten hinterher schaufeln und fertig.

Git lässt sich bedienen indem du in dem Order in dem die .git Datei ist was verändert/gespeichert hast, in Vscode in dem 'Source Control' Panel auf der linken Seite die Änderungen über das '+' stagest und mit dem Häkchen am oberen Rand alle 'staged changes' comittest. Da wird erstmal ein Textfeld auf ploppen in dem du die vorgenommenen Änderungen eintragen kannst, und dann kannst du über das Syncronisations-icon am unteren Rand von Vscode das Projekt synchronisierst.

Es ist außerdem praktisch dann noch mit dem Shell Befehl 'git config --global credential.helper store' dafür zu sorgen dass du die Anmeldedaten für Gitlab nur einmal eingeben musst und sie dann gespeichert sind.

Obendrein empfehle ich in Vscode mit strg+k+s zum 'Keyboard Shortcut' Menü zu gehen und 'Git:Stage All Changes' und 'Git:Commit Staged' jeweils mit Shortcuts zu versehen.
