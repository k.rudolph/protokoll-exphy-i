\section{Theorie}

    \subsection{Trägheitsmoment}

    Das Trägheitsmoment eines Körpers bei Rotation um eine Achse ist durch \cite[S. 134]{Dem1} 
    \begin{equation}
        \Theta = \int_V r_\perp^2 \dd m = \int_V r_\perp^2 \rho \dd V\text{,}
    \end{equation}
    mit dem senkrechten Abstand \( r_\perp \) des Massenelementes \( \dd m \) von der Rotationsachse und Dichte \( \rho \), gegeben. 
    Das Trägheitsmoment verschiedener Körper mit homogener Massenverteilung bezüglich einer Achse durch den Schwerpunkt ist in Tabelle \ref{tab:Momente} angegeben. 
    Hierbei bezeichnet \( M \) die Gesamtmasse des Körpers, \( R \) den Radius und \( L \) die Länge, b.z.w. \( R_i \) den inneren und \( R_a \) den äußeren Radius 
    des Hohlzylinders und \( m_1 \) und \( m_2 \) die beiden Massen des Hantelkörpers.
  
    \begin{table}[ht]
        \centering
        \caption{Trägheitsmomente verschiedener Körper mit homogener Massenverteilung bei Rotationen um eine Schwerpunktachse \cite[S. 137]{Dem1}.}
        \begin{tabular}{|c|c|}
            \hline\textbf{Körper} & \textbf{Trägheitsmoment}\\
            \hline
             Dünne Scheibe &  $\frac{1}{2}MR^2$ \\ \hline
             Vollzylinder & $\frac{1}{2}MR^2$ \\ \hline
             Hohlzylinder &  $\frac{1}{4}M(R_a^2+R_i^2)$ \\ \hline
             Stab um Schwerpunkt & $\frac{1}{12}ML^2$ \\ \hline
             Kugel & $\frac{2}{5}MR^2$ \\ \hline
             Würfel & $\frac{1}{6}ML^2$ \\ \hline
             Hantelkörper & $\frac{m_1m_2}{m_1+m_2}R^2$ \\ \hline
        \end{tabular}
        \label{tab:Momente}
    \end{table}

    Ist das Trägheitsmoment bezüglich einer Achse \( S \) durch den Schwerpunkt bekannt, so lässt sich das Trägheitsmoment bezüglich einer zu dieser Achse parallelen Achse 
    \( B \) im Abstand \( a \) nach dem Steinerschen Satz als \cite[S. 134]{Dem1} 
    \begin{equation}
        \Theta_B = \Theta_S + Ma^2 \text{,}
        \label{eq:Steiner}
    \end{equation}
    wobei \( M \) die Gesamtmasse des Körpers bezeichnet.
    
    Um das Trägheitsmoment eines Körpers bezüglich beliebiger Achsen darzustellen, betrachtet man den vektoriellen Zusammenhang zwischen Drehimpuls und Winkelgeschwindigkeit. 
    Dieser lässt sich durch den Trägheitstensor \( \tilde{\Theta} \) darstellen. Diagonalisieren führt diesen in seine Hauptachsenform über, wobei die Diagonalelemente 
    \( \Theta_a,\ \Theta_b,\ \Theta_c \) als Hauptträgheitsmomente bezeichnet werden. Die zugehörigen Hauptträgheitsachsen spannen ein Koordinatensystem \( [\xi,\eta,\zeta] \) 
    auf, mithilfe dessen man das Trägheitsmoment für alle Achsen in Form des Trägheitsellipsoiden \cite[S. 142]{Dem1}
    \begin{equation}
        \xi^2\Theta_A+\eta^2\Theta_B+\zeta^2\Theta_C = 1
    \end{equation}
    darstellen kann.


    \subsection{Drehschwingung}

    Experimentell lässt sich das Trägheitsmoment aus der Schwingungsdauer einer Drehschwingung bestimmen. Das rücktreibende Drehmoment einer Schneckenfeder bei Auslenkung um 
    \( \varphi \) ist durch \cite[S. 139]{Dem1}
    \begin{equation}
        D = - D_r \varphi 
        \label{eq:Drehmoment}
    \end{equation}
    mit Winkelrichtgröße \( D_r \) gegeben. Die Schwingungsdauer \( T \) eines an dieser Feder schwingenden Körpers mit Trägheitsmoment \( \Theta \) ist durch \cite[S. 139]{Dem1}
    \begin{equation}
        T = 2\pi \sqrt{\frac{\Theta}{D_r}} 
    \end{equation}
    bestimmt, woraus sich für das Trägheitsmoment bezüglich der Schwingungsachse 
    \begin{equation}
        \Theta = \frac{T^2}{4\pi^2} D_r
        \label{eq:TragheitSchwingung}
    \end{equation}
    ergibt.



    \subsection{Winkelbeschleunigung}

    Das Trägheitsmoment lässt sich ebenfalls aus der Winkelbeschleunigung bestimmen. Das durch eine im Abstand \( r \) zur Drehachse wirkenden Kraft \( F \), welche nach 
    \( F = mg \) \cite[S. 50]{Dem1} durch die Schwerebeschleunigung einer Masse \( m \) bei Erdbeschleunigung \( g \) erzeugt wird, erzeugte Drehmoment \( D \) ist 
    durch \cite[S. 133]{Dem1}
    \begin{equation}
        D = r \cdot F = mgr
    \end{equation}
    gegeben. Für das durch die Winkelbeschleunigung \( \dot{\omega} \) erzeugte Drehmoment gilt \cite[S. 138]{Dem1}
    \begin{equation}
        D = \Theta \dot{\omega}\text{,}
        \label{eq:Drehmoment2}
    \end{equation}
    woraus unter Verwendung von \( \dot{\omega} = \frac{a}{R} \) \cite[vgl. S. 42]{Dem1} als Beziehung zwischen \( \dot{\omega} \) und der Beschleunigung \( a \) im 
    Radius \( R \) 
    \begin{equation}
        \Theta = \frac{rRmg}{a}
        \label{eq:Winkel}
    \end{equation}
    folgt.

    \subsection{Physikalisches Pendel}

    Als physikalisches Pendel bezeichnet man einen Körper, welcher um eine Achse, die nicht durch seinen Schwerpunkt verläuft, schwingt. Das rücktreibende Drehmoment ist hier 
    bei Auslenkung um der Winkel \( \varphi \) durch \cite[S. 86]{Gerthsen}
    \begin{equation}
        D = -rmg \sin(\varphi)
    \end{equation}
    mit Abstand \( r \) zwischen Pendel- und Schwerpunktachse, Körpermasse \( m \) und Erdbeschleunigung \( g \) gegeben. Unter Verwendung von Gleichung \eqref{eq:Drehmoment2} 
    und der Kleinwinkelnäherung führt dies zu 
    \begin{equation}
        \ddot{\varphi} = \dot{\omega} = -\frac{rmg}{\Theta}\varphi \text{.}
        \label{eq:DGL}
    \end{equation}
    Diese Bewegungsgleichung gleicht der eines mathematischen Pendels, woraus für das physikalische Pendel die Schwingungsfrequenz \cite[S. 87]{Gerthsen}
    \begin{equation}
        \omega = \sqrt{\frac{mgr}{\Theta}}
        \label{eq:}
    \end{equation}
    und daraus unter Verwendung von \( \omega = \frac{2\pi}{T} \) \cite[S. 337]{Dem1} für das Trägheitsmoment 
    \begin{equation}
        \Theta = \frac{T^2gzm}{4\pi^2}
        \label{eq:Pendel}
    \end{equation}
    folgt.