\section{Auswertung}

\subsection{Teil A: Trägheitsmoment aus Drehschwingungen}
\subsubsection{Bestimmung der Winkelrichtgröße}
 In den Abbildungen \ref{fig:Winkelrichtgröße rechts} und \ref{fig:Winkelrichtgröße links} ist die Winkelauslenkung in Abhängigkeit des angreifenden Drehmoments aufgetragen.
 Es wurden nur fünf Messwerte aufgetragen, da sich die Spiralfeder bei den letzten Messungen - mit höherer Masse - zu weit ausgelenkt hatte. Es wird ein Ablesefehler des Winkels von 5° angenommen und ein Messfehler von 0,005m beim Ausmessen des Radius der Scheibe. Die Gewichte sind hier nicht fehlerbehaftet. Ihre Masse wird als exakt angenommen. Es existieren also nur zwei Fehlergrößen: Der abgelesene Winkel (in der Gleichung als $w$ bezeichnet und in Radiant angegeben) und der Radius $r$. Man erhält als Fehler der Winkelrichgröße (gemittelt) $0,0028 \mathrm{Nm}\cdot\mathrm{rad}^{-1}$, was mit folgender Gleichung \ref{WinkelrichgrößeGleichung} berechnet wurde:
 
\begin{equation}
\centering
\sigma^2_{D} = \sigma^2_{r}\cdot\left(\frac{ma}{w}\right)^2+\sigma^2_{w}\cdot\left(\frac{-rma}{w^2}\right)^2 
\label{WinkelrichgrößeGleichung}
\end{equation}
 
Python gibt als Steigung der linearen Regression durch die Messwerte (linregress) $m= 0,0172$ an. Es ergibt sich für die Winkelrichtgröße also $D_r=(0,0172\pm0,0028)\mathrm{Nm}\cdot\mathrm{rad}^{-1}$
 
\begin{figure}[H] 
\centering
 \includegraphics[width=\textwidth]{images/rechts.png}
 \caption{Messung der Winkelrichtgröße $D_r$ für ein Drehmoment nach rechts; Fit: Lineare Regression durch die Messwerte}
 \label{fig:Winkelrichtgröße rechts}
 \end{figure}
 
 \begin{figure}[H]
 \centering
 \includegraphics[width=\textwidth]{images/links.png}
  \caption{Messung der Winkelrichtgröße $D_r$ für ein Drehmoment nach links; Fit: Lineare Regression durch die Messwerte}
 \label{fig:Winkelrichtgröße links}
 \end{figure}
 
 \subsubsection{Bestimmung der Trägheitsmomente}\label{sec:Tragheit}
 Die Trägheitsmomente der Versuchskörper können über die mittlere Schwingungsdauer $T$ und die Winkelrichtgröße $D_r$ bestimmt werden. Den Zeitmessfehler schätzen wir auf 
 $0,5$ Sekunden (auf zehn Schwingungen). Für eine Schwingung ergibt sich ein Fehler von $0,05$ Sekunden. Der Fehler der Winkelrichtgröße $D_r$ beträgt $0,0028\ \mathrm{Nm}$. 
 Man erhält als Fehler für $\theta$ aus der Gaußschen Fehlerfortpflanzung:
 \begin{equation}
 \centering
  \sigma^2_{\theta} = \sigma^2_{D}\cdot\left(\frac{T^2}{4\pi^2}\right)^2+\sigma^2_{T}\cdot\left(\frac{DT}{2\pi^2}\right)^2   
  \label{Trägheitsmomente Gleichung}
 \end{equation}
 

\begin{table}[h]
 \caption{Trägheitsmomente errechnet (\ref{Trägheitsmomente Gleichung}) aus Schwingungsdauern (experimentell)}
    \centering
    \begin{tabular}{c|c|c|c}
    
    \textbf{Körper}&\textbf{Schwingungsdauer $T$ in s}&\textbf{$\theta$ in $10^{-4}\ \mathrm{kgm^2}$}&\textbf{$\sigma$ in $10^{-4}\ \mathrm{kgm^2}$}\\\hline
    
    Hohlzylinder&0,98$\pm$0,05&4,2&0,8 \\
    Vollzylinder&0,72$\pm$0,05&2,3&0,5\\
    Stab Achse 1 (mittig)&2,20$\pm$0,05&21,1&3,6\\
    Stab Achse 2&2,54$\pm$0,05&28,2&4,8\\
    Kugel&1,02$\pm$0,05&4,6&0,9\\
    Würfel&0,99$\pm$0,05&4,3&0,9\\
    Würfel (Ecke)&0,96$\pm$0,05&4,1&0,8\\
    Hanteln&2,97$\pm$0,05&38,5&7\\
    \end{tabular}
    \label{tab:Trägheitsmomente experimentell}
\end{table}

Die Trägheitsmomente können auch theoretisch anhand der Körpereigenschaften nach Tabelle \ref{tab:Momente} errechnet werden. Dafür sind nur die Massen und die Maße der Körper nötig, die vorher gemessen wurden (Schwingungsdauer ist nicht erforderlich). Diese haben ebenfalls einen Fehler. Die Waage hat einen Fehler von 0,5 Gramm und der Messfehler beim Ausmessen der Körper wird auf 5 Millimeter geschätzt. Aus den Formeln aus Tabelle \ref{tab:Momente} folgt:

\begin{table}[H]
\caption{Trägheitsmomente errechnet aus Körpereigenschaften (theoretisch)}
    \centering
    \begin{tabular}{c|c|c}
    \textbf{Körper}&\textbf{$\theta$ in $10^{-4}\ \mathrm{kgm^2}$}\\\hline
    
    Hohlzylinder&4,8$\pm$0,9\\
    Vollzylinder&2,8$\pm$0,7\\
    Stab Achse 1 (mittig)&35$\pm$7\\
    Stab Achse2&45,8$\pm$7,13\\
    Kugel&5,9$\pm$1,08\\
    Würfel&5$\pm$0,7\\
    Würfel (Ecke)&5$\pm$0,7\\
    Hanteln&69,8$\pm$7,5\\
    \end{tabular}
    
    \label{tab:Trägheitsmomente theoretisch}
\end{table}

 \subsubsection{Trägheitsellipse des Tischchens}
 Aus der Vermessung der Schwingungsdauer des Tischchens folgen für verschiedene Winkel unterschiedliche Trägheitsmomente (nach derselben Formel wie (\ref{Trägheitsmomente Gleichung})). Im Polardiagramm \ref{fig:Trägheitsellipse} wurden die reziproken Quadratwurzeln der Trägheitsmomente $\left(\frac{1}{\sqrt{\theta}}\right)$ über 
 dem entsprechenden Winkel aufgetragen. Grafisch gut zu erkennen sind die beiden Hauptträgheitsachsen bei etwa $\theta_{A}=15$° und $\theta_{B}=120$°.
 
  \begin{figure}[H]
 \centering
 \includegraphics[width=\textwidth]{images/Trägheitsellipse.png}
  \caption{Trägheitsellipse (rot: Messwerte, blau: gespiegelte Messwerte)}
 \label{fig:Trägheitsellipse}
 \end{figure}

\subsection{Teil B: Trägheitsmoment aus Winkelbeschleunigung}
\subsubsection{Berechnung des Trägheitsmoments aus der Beschleunigung des Rades}
Für Teil B des Versuches wird zuerst der erste Teil, in dem das Rad durch angehängte Massen beschleunigt wird, betrachtet. 

Die Messwerte sollen dazu genutzt werden, das Trägheitsmoment $\Theta$ zu berechnen. Die Formel dazu lautet:\\
\begin{equation}
    \Theta = \frac{r R m g}{a} - m r^2\\
    \label{eq:Winkelbeschleunigung}
\end{equation}

Diese Formel wurde im Theorieteil hergeleitet(\ref{eq:Winkel}). Der Term $m r^2$ muss dabei
abgezogen werden, da dadurch das Trägheitsmoment der Beschleunigung des Bindfadenrades subtrahiert wird, das sonst das Ergebnis verfälschen würde.\\
Die Erdbeschleunigung $g$ wird mit $g = 9,81$ $\frac{m}{s^2}$ als exakt angenommen. $R$ und $r$ sind beim Experiment mit $r = \SI{0,0637\pm 0,008}{\m}$ und $R = \SI{0,1830\pm 0,008}{m}$ gemessen worden. $m$ variiert und $a$ kann in Abhängigkeit von $m$ aus den gemessenen Abständen berechnet werden. Es gilt $a = \Dot{\omega}$. Dabei ist $\omega$ die Winkelgeschwindigkeit. 
Um die Winkelbeschleunigung zu bestimmen, wird also die Winkelgeschwindigkeit über die Zeit aufgetragen, um die Steigung des Graphen zu bestimmen. Die Steigung sollte optimalerweise konstant sein und entspricht der Winkelgeschwindigkeit. Um aus den gemessenen Werten die Winkelgeschwindigkeit bestimmen zu können wird mit 
$v = \frac{s}{t}$ der Abstand der Markierungen durch die Zeit $(0,25\ \mathrm{s})$ geteilt. 
So ergeben sich die Graphen in Abbildung \ref{Winkelgeschwindigkeiten Graph}, deren Steigung entspricht $a$.\\
\vspace{10pt}
\begin{figure}[h]
    \centering
    \includegraphics[width=1\textwidth]{images/Winkelgeschwindigkeiten.png}
    \caption{Winkelgeschwindigkeiten in Abhängigkeit von der Masse. Fit durch\\ Lineare Regression}
    \label{Winkelgeschwindigkeiten Graph}
\end{figure}
\vspace{5pt}
Dann lassen sich die Steigungen aus dem Graphen ablesen:
\begin{table}[h]
    \centering
    \caption{Winkelbeschleunigungen für die einzelnen Massen}
    \begin{tabular}{c|c}
    Masse&Winkelbeschleunigung in $\mathrm{\frac{m}{s^2}}$  \\
    \hline
    0,1 kg&$0,325\pm 0,004$\\
    0,2 kg&$0,633\pm 0,004$\\
    0,5 kg&$0,98\pm 0,004$\\
    1 kg&$2,267\pm 0,004$\\
    \end{tabular}

    \label{Winkelbeschleunigungen}
\end{table}

Jetzt sind alle notwendigen Größen zur Berechnung des Trägheitsmoments vorhanden. Durch die Messreihen lassen sich jetzt also Werte für das Trägheitsmoment nach 
\eqref{eq:Winkelbeschleunigung} bestimmen.
\begin{align*}
\Theta_{0,1\ \mathrm{kg}} = (0,035 \pm 0,005) \mathrm{m^2kg}\\
\Theta_{0,2\ \mathrm{kg}} = (0,035 \pm 0,005) \mathrm{m^2kg}\\
\Theta_{0,5\ \mathrm{kg}} = (0,056 \pm 0,007) \mathrm{m^2kg}\\
\Theta_{1\ \mathrm{kg}} = (0,046 \pm 0,006) \mathrm{m^2kg}
\end{align*}

Die Fehler wurden dabei mithilfe der Gauß´schen Fehlerfortpflanzung bestimmt. Die Formel dazu lautet:
\begin{equation}
     \sigma_\Theta=\sqrt{\sigma_r^2(\frac{\partial \Theta}{\partial r})^2+\sigma_R^2(\frac{\partial \Theta}{\partial R})^2+\sigma_a^2(\frac{\partial \Theta}{\partial a})^2}\\
\end{equation}

Dadurch ergibt sich dann ein gemitteltes Gesamtergebnis für Teil 1 von:
\begin{equation}
    \mathbf{\Theta=(0,043\pm0,006)\ \mathrm{m^2kg}}\\
    \nonumber
\end{equation}

\subsubsection{Berechnung des Trägheitsmoments aus der Schwingungsdauer des physikalischen Pendels}
Als zweites wird der Versuchsteil betrachtet, in dem die Periodendauer des physikalischen Pendels gemessen wurde. Auch mit diesen Messwerten kann das Trägheitsmoment 
mithilfe folgender Formel berechnet werden (vgl. \eqref{eq:Pendel}):
\begin{equation}
    \Theta = \frac{T^2 g z m}{4 \pi^2} - m z^2
\end{equation}

Dabei bezeichnet $T$ die Periodendauer der Schwingung, $g$ die Erdbeschleunigung, $z$ die Entfernung des Zusatzgewichts vom Radmittelpunkt und $m$ die Masse des Zusatzgewichtes\\
Auch diese Formel wurde bereits im Theorieteil hergeleitet, der Term $m z^2$ wird hier vom eigentlichen Berechnungsterm für das Trägheitsmoment abgezogen, um das Trägheitsmoment des Zusatzgewichtes aus dem Ergebnis auszunehmen.\\
Alle benötigten Werte sind entweder konstant oder im Experiment gemessen worden, sodass mithilfe der Formel das Trägheitsmoment sofort ausgerechnet werden kann. 
Im Experiment wurden gemessen:\\

$T_1 = (2,049 \pm 0,1) \mathrm{s} $,
$z_1 = (136 \pm 5) \mathrm{mm} $;\\
$T_2 = (2,013 \pm 0,1) \mathrm{s} $,
$z_1 = (136 \pm 5) \mathrm{mm} $;\\
$m = (0,2663 \pm 0,0005) \mathrm{kg}$\\

Somit kann nun in die Formel eingesetzt werden:
\begin{align*}
    \Theta_1 &= (0,033 \pm 0,005) \mathrm{m^2kg}\\
    \Theta_2 &= (0,032 \pm 0,004) \mathrm{m^2kg}
\end{align*}



Für Teil 2 erfolgt die Fehlerrechnung wie in Teil 1 mithilfe der Gauß´schen Fehlerfortpflanzung, die hier folgende Formel produziert:
\begin{equation}
    \sigma_\Theta=\sqrt{\sigma_T^2(\frac{\partial \Theta}{\partial T})^2+\sigma_z^2(\frac{\partial \Theta}{\partial z})^2+\sigma_m^2(\frac{\partial \Theta}{\partial m})^2}\\
\end{equation}

Für Teil 2 des Versuchs ergibt sich dann ein gemitteltes Gesamtergebnis von:
\begin{equation}
    \nonumber
    \mathbf{\Theta=(0,032\pm0,004)\ \mathrm{m^2kg}}\\
\end{equation}

Die Ergebnisse von Teil 1 und 2 weichen dabei etwas voneinander ab.