\section{Theorie}

    \subsection{Trägheitsmoment}

    Das Trägheitsmoment \( I \) eines Körpers bei Rotation um eine Achse ist durch \cite[S. 134]{Dem1} 
    \begin{equation}
        I = \int_V r_\perp^2 \dd m = \int_V r_\perp^2 \rho \dd V\text{,}
    \end{equation}
    mit dem senkrechten Abstand \( r_\perp \) des Massenelementes \( \dd m \) von der Rotationsachse und Dichte \( \rho \), gegeben.

    Für das Trägheitsmoment einer homogenen Kreisscheibe mit Masse \( m \), Radius \( r \) und Dicke \( d \) durch die Längsachse gleicht dem eines Vollzylinders mit Höhe 
    \( h=d \) und ist daher durch \cite[S. 137]{Dem1} 
    \begin{equation}
        I_\mathrm{L} = \frac{1}{2} m r^2
        \label{eq:Längs}
    \end{equation}
    gegeben. Für eine Rotation um die Querachse gilt \cite[S. 41]{MHF}
    \begin{equation}
        I_\mathrm{Q} = \frac{1}{4}mr^2+\frac{1}{12}md^2.
        \label{eq:Quer}
    \end{equation}

    Das Trägheitsmoment durch zu diesen Schwerpunktachsen parallele Achsen im Abstand \( a \) können nach dem Steinerschen Satz  \cite[S. 134]{Dem1} 
    \begin{equation}
        I = I_S + Ma^2
        \label{eq:Steiner}
    \end{equation}
    bestimmt werden, wobei \( M \) die Gesamtmasse des Körpers bezeichnet.


    \subsection{Physikalisches Pendel}

    Als physikalisches Pendel bezeichnet man einen Körper, welcher um eine Achse, die nicht durch seinen Schwerpunkt verläuft, schwingt. Das rücktreibende Drehmoment \( D \) ist hier 
    bei Auslenkung um der Winkel \( \varphi \) durch \cite[S. 86]{Gerthsen}
    \begin{equation}
        D = -rmg \sin(\varphi)
    \end{equation}
    mit Abstand \( r \) zwischen Pendel- und Schwerpunktachse, Körpermasse \( m \) und Erdbeschleunigung \( g \) gegeben. Unter Verwendung von \cite[S. 138]{Dem1}
    \begin{equation}
        D = I \dot{\omega} 
    \end{equation} 
    und der Kleinwinkelnäherung führt dies zu 
    \begin{equation}
        \ddot{\varphi} = \dot{\omega} = -\frac{rmg}{I}\varphi \text{.}
        \label{eq:DGL}
    \end{equation}
    mit Winkelgeschwindigkeit \( \omega \). 
    Diese Bewegungsgleichung gleicht der eines mathematischen Pendels, woraus für das physikalische Pendel die Schwingungsfrequenz \cite[S. 87]{Gerthsen}
    \begin{equation}
        \omega = \sqrt{\frac{mgr}{I}}
        \label{eq:}
    \end{equation}
    und daraus unter Verwendung von \( \omega = \frac{2\pi}{T} \) \cite[S. 337]{Dem1} mit Schwingungsdauer \( T \) für das Trägheitsmoment 
    \begin{equation}
        I = \frac{T^2grm}{4\pi^2}
        \label{eq:Pendel}
    \end{equation}
    folgt.

    \subsection{Kreisel}

    Als Kreisel wird ein drehender Körper, welcher nur in einem Punkt festgehalten wird, bezeichnet. Ein Kreisel, auf welchen kein Drehmoment wirkt, wird kräftefreier 
    Kreisel genannt. 

        \subsubsection{Nutation}
        Rotiert ein symmetrischer Kreisel um seine Hauptträgheitsachse, welche auch als Figurenachse bezeichnet wird, so fallen seine momentane Drehachse und seine Drehimpulsachse 
        mit der Figurenachse zusammen. Wird auf diesen Kreisel für kurze Zeit eine Kraft ausgeübt, so ändern sich seine momentane Drehachse und damit auch seine Drehimpulsachse. 
        Der Drehimpuls \( \vec{L} \) des Kreisels kann nun nach \cite[S. 91]{Gerthsen} 
        \begin{equation}
            \vec{L} = \vec{L}_\perp + \vec{L}_\parallel = I_\perp \vec{\omega}_\perp + I_\parallel \vec{\omega}_\parallel
            % \label{eq:}
        \end{equation}
        in einen Anteil senkrecht und einen Anteil parallel zur Figurenachse aufgeteilt werden. \( \vec{L}_\parallel \) beschreibt hierbei die Rotation um die Figurenachse mit 
        Winkelgeschwindigkeit \( \omega_R = \omega_\parallel \), \( \vec{L}_\perp \) bewirkt eine Rotation der Figurenachse um die Drehimpulsachse, welche als Nutation 
        mit Nutationswinkelgeschwindigkeit \( \omega_N = \omega_\perp \) bezeichnet wird. Der Öffnungswinkel \( \alpha \) des durch die Figurenachse beschriebenen 
        Nutationskegel ist durch \cite[S. 91]{Gerthsen}
        \begin{equation}
            \tan(\alpha) = \frac{I_\perp \omega_\perp}{I_\parallel \omega_\parallel}
            % \label{eq:}
        \end{equation}
        gegeben.

        \subsubsection{Präzession}
        Wirkt auf einen Kreisel ohne Nutation ein äußeres Drehmoment \( D \), so erzeugt dies ein Drehmoment, welches senkrecht zur Figurenachse wirkt und der Kreisel beginnt sich 
        um seinen Aufhängepunkt zu drehen. Diese Bewegung wird als Präzession bezeichnet. Die Präzessionswinkelgeschwindigkeit \( \omega_P \) ist dabei durch \cite[S. 148]{Dem1} 
        \begin{equation}
            \omega_P = \frac{D}{L}
            \label{eq:omPrä}
        \end{equation}
        gegeben. 

        Wird der Kreisel durch Anbringen eines zusätzlichen Gewichtes mit Masse \( m \) um den Winkel \( \theta \) aus seiner Normallage ausgelenkt, so bewirkt dies ein 
        Drehmoment \( D = mgr \sin(\theta) \)\cite[vgl. S. 148]{Dem1} mit Erdbeschleunigung \( g \) und Abstand \( r \) zwischen Aufhängepunkt und Gewicht. Unter Verwendung 
        des Drehimpulses \( L = I \omega_R \sin(\theta) \)\cite[vgl. S. 148]{Dem1} des Kreisels ergibt sich Gleichung \eqref{eq:omPrä} zu \cite[S. 148]{Dem1}
        \begin{align}
            \omega_P &= \frac{mgr \sin(\theta)}{I \omega_R \sin(\theta)} = \frac{mgr}{I \omega_R}\\
            \implies I &= \frac{mgr}{\omega_P \omega_R}.
            \label{eq:Präzession}
        \end{align}
