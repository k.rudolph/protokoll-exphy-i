\section{Auswertung}
Alle Fehler werden mithilfe der Gauß´schen Fehlerfortpflanzung berechnet. Die verwendete Formel lautet:
\begin{equation*}
    \sigma_f = \sqrt{\sigma_a^2 \left ( \frac{\partial f}{\partial a}\right )^2 + \sigma_b^2 \left ( \frac{\partial f}{\partial b}\right )^2 + ...}
\end{equation*}
Dabei bezeichnet f die zu berechnende Größe, a,b,... die fehlerbehafteten, zur Berechnung von f genutzen Größen und $\sigma_x$ den Fehler von x. 
\subsection{Trägheitsmomente des Rades}
Zuerst wird das Trägheitsmoment des Rades bestimmt. Dazu werden die Formeln für das Trägheitsmoment eines flachen Rades entlang der Längs- und Querachse verwendet (\eqref{}):
\begin{align*}
     I_L &= \frac{1}{2} m r^2\\
     I_Q &= \frac{1}{4} m r^2+\frac{1}{12}md^2
\end{align*}
Dabei bezeichnet $m$ die Masse, $r$ den Radius des Rades. $I$ ist das zugehörige Trägheitsmoment, $d$ ist der Dicke des Rads. Die Masse $m$ wird dabei als exakt angenommen. Somit ergibt sich für das Trägheitsmoment um die horizontale Achse ein Wert von:
\begin{equation*}
     I_{hor} = I_L = (0,01447 \pm 0,00010)\mathrm{kg\cdot m^2}
\end{equation*}
Dabei wird der Fehler berechnet durch:
\begin{equation*}
     \sigma_I = \sqrt{\sigma_r^2 \left ( mr\right )^2} 
\end{equation*}
mit $\sigma_r = 0,5\mathrm{mm}$, da r mit einem millimetergenauen Maß gemessen wurde.\\
Für das Trägheitsmoment um die vertikale Achse müssen die einzelnen Trägheitsmomente des Rades und des Ausgleichsgewichtes berücksichtigt werden. Da die räumliche Ausdehnung des Gewichtes im Vergleich zu der des Rades und der Strecke zur Rotationsachse relativ gering ist, wird es dabei als Punktmasse betrachtet. Dazu hat das Rad senkrecht zu seiner Symmetrieachse ein etwas höheres Trägheitsmoment, das durch seine Dicke begründet ist. Somit ergibt sich mithilfe des Satzes von Steiner (\eqref{}):
\begin{align*}
    I_{ver} &= I_{Rad} + I_{Gewicht}\\
    \Leftrightarrow I_{ver} &= \frac{1}{4} m_{Rad} r^2 + m_{Rad} a_{Rad}^2 + \frac{1}{12} m_{Rad} d_{Rad}^2+ m_{Gewicht} a_{Gewicht}^2
\end{align*}
Dabei bezeichnet $m$ die jeweiligen, als exakt angenommenen Massen des Rads oder des Gewichts, $a$ die Abstände der Massenschwerpunkte von der Rotationsachse und $r$ den Radius des Rads sowie $d$ die Dicke des Rads. Es ergibt sich:
\begin{equation*}
    I_{ver} = (0,0592 \pm 0,0003)\mathrm{kg \cdot m^2}
\end{equation*}
Die Abstände wurden alle mithilfe eines millimetergenauen Maßes bestimmt, somit ergibt sich für die Fehler $\sigma_{r} = \sigma_{a_{Rad}} = \sigma_{d_{Rad}} = \sigma_{a_{Gewicht}} = 0,5 \mathrm{mm}$. Die Formel zur Fehlerberechnung lautet dann:
\begin{equation*}
     \sigma_I = \sqrt{\sigma_r^2 \left ( \frac{mr}{2}\right )^2+\sigma_{a_{Rad}}^2 \left ( 2ma_{Rad}\right )^2+\sigma_{d_{Rad}}^2 \left ( \frac{md_{Rad}}{6}\right )^2+\sigma_{a_{Gewicht}}^2 \left ( 2ma_{Gewicht}\right )^2} 
\end{equation*}
\subsection{Trägheitsmoment des Kreisels aus der Pendelschwingung}
Als nächstes kann man aus den Messungen des ersten Teils des Versuches, der Schwingung des physikalischen Pendels, das Trägheitsmoment des Kreisels berechnet. Dazu wird folgende Formel verwendet (\eqref{}):
\begin{equation*}
    I = \frac{T^2 g r m}{4 \pi^2} - m r^2
\end{equation*}
$I$ bezeichnet dabei erneut das Trägheitsmoment. $T$ gibt die Periodendauer der Schwingung an, $g$ ist die Erdbeschleunigung, $r$ die Entfernung des Gewichts vom Rotationsmittelpunkt, hier ist $r$ der Radius des Rades, $m$ bezeichnet die Masse des Gewichtes. Da für $T$ sechs Messungen durchgeführt wurden, wird das arithmetische Mittel der Werte verwendet. Da $g$ Naturkonstante ist, ist diese Größe nicht fehlerbehaftet, $m$ wird erneut als exakt betrachtet. Damit ergibt sich:
\begin{equation*}
    I = (0,013 \pm 0,009)\mathrm{kg\cdot m^2}
\end{equation*}
Hier lautet die Formel zur Fehlerberechnung:
\begin{equation*}
    \sigma_I = \sqrt{\sigma_T^2 \left ( \frac{2Tgrm}{4\pi^2}\right )^2+\sigma_r^2 \left ( \frac{T^2 gm}{4 \pi^2}- 2mr\right )^2}
\end{equation*}
Für eine manuelle Zeitmessung ist ein Fehler von $0,5\mathrm{s}$ angesetzt, da allerdings 10 Schwingungen gemessen wurden, ist $\sigma_T = \frac{0,5 \mathrm{s}}{10} = 0,05 \mathrm{s}$. Da r mit einem millimetergenauen Maß gemessen wurde, ist wieder $\sigma_r = 0,5 \mathrm{mm}$.
\subsection{Trägheitsmoment des Rades aus der Präzessionsbewegung}
Für die Präzessionsbewegung des Kreisels wurden die Präzessionsfrequenz und die Rotationsfrequenz gemessen. In Abbildung \ref{Im: Kreiselpräzession omega P} wurden die Präzessionsfrequenzen über die Inversen der Rotationsfrequenzen aufgetragen. 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/Kreiselpräzession omega p.png}
    \caption{Präzessions- und Rotationsfrequenz für verschiedene Zusatzgewichte}
    \label{Im: Kreiselpräzession omega P}
\end{figure}
Aus den Graphen lässt sich nun die Steigung $\omega$ bestimmen. Es ergibt sich\\ $\omega_P = \omega \cdot \frac{1}{\omega_R} \Leftrightarrow \omega = \omega_P \cdot \omega_R$. Das Trägheitsmoment berechnet man dann nach Formel \eqref{}
\begin{equation*}
    I = \frac{rmg}{\omega_P \omega_R} = \frac{rmg}{\omega}
\end{equation*}
mit den Steigungen $\omega_{20g} = (5,143 \pm 0,001) \frac{1}{\mathrm{s^2}}, \omega_{40g} = (10,952 \pm 0,001) \frac{1}{\mathrm{s^2}}, \omega_{60g} = (18,667 \pm 0,001) \frac{1}{\mathrm{s^2}}$ und den Größen  $m$ als Masse und $r$ als Abstand des Zusatzgewichtes zur Drehachse, und $g$ als Erdbeschleunigung. Auch hier werden $g$ und $m$ nicht als fehlerbehaftet betrachtet. Somit ergeben sich Trägheitsmomente von:
\begin{align*}
    I_{20g} &= (0.01030 \pm 0,00002) kg \cdot m^2\\
    I_{40g} &= (0.00967 \pm 0,00002) kg \cdot m^2\\
    I_{60g} &= (0.00851 \pm 0,00002) kg \cdot m^2\\
\end{align*}
Die Fehler wurden dabei mithilfe der Formel 
\begin{equation*}
    \sigma_I = \sqrt{\sigma_r^2 \left ( \frac{gm}{\omega}\right )^2+\sigma_{\omega}^2 \left ( \frac{-rmg}{\omega^2}\right )^2}
\end{equation*}
berechnet, mit einem Fehler $\sigma_r=0,5\mathrm{mm}$, da $r$ mithilfe eines millimetergenauen Maßes bestimmt wurde. Somit ergibt sich durch das arithmetische Mittel der oben berechneten Werte ein Trägheitsmoment von
\begin{equation*}
    I = (0,00949 \pm 0,00002)\mathrm{kg}\cdot\mathrm{m^2}
\end{equation*}
\subsection{Vergleich der Trägheitsmomente}
Sowohl die für das physikalische Pendel und die Kreiselpräzession berechneten Werte als auch der theoretisch berechnete Wert für das Trägheitsmoment um die horizontale Achse stimmen ungefähr überein. Der theoretisch berechnete Wert ist dabei geringfügig größer. Dies hat wahrscheinlich den Grund, dass die praktisch bestimmten Werte Fehlern wie der Reibung unterliegen. Dadurch fallen diese geringfügig kleiner aus als der tatsächliche, unter Optimalbedingungen geltende Wert.
\subsection{Nutation}
\label{sec: Nutation}
In Abbildung \ref{Im: Kreiselnutation} wurden  die gemessenen Nutationsfrequenzen gegen die jeweiligen Rotationsfrequenzen aufgetragen: 
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\textwidth]{images/Kreiselpräzession Nutation.png}
    \caption{Zusammenhang zwischen der Rotationsfrequenz $\omega_R$ und der Nutationsfrequenz $\omega_N$ des Kreisels}
    \label{Im: Kreiselnutation}
\end{figure}
Man erkennt einen proportionalen Zusammenhang. Die Proportionaltitätskonstante $k$ mit $\omega_N = k \cdot \omega_R$ beträgt $k = \frac{\omega_N}{\omega_R} = 0,15 \pm 0,10$. Diese Proportionalitätskonstante stimmt etwa mit dem Quotienten des horizontalen und vertikalen Trägheitsmoments überein: $\frac{I_{hor}}{I_{ver}} = 0,2323 \pm 0,0011$.