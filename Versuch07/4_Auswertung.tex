\section{Auswertung}

    \subsection{Druckkurve}

    Aus den gemessenen Widerständen wird nach Formel \eqref{eq: Pt1000 Widerstandsthermometer} die Temperatur \( \vartheta \) als
    \begin{equation}
        \vartheta = \frac{A-\sqrt{A^2-4B\left( \frac{R\left( \vartheta \right)}{R_0} -1 \right)}}{2B}
        % \label{eq:}
    \end{equation}
    bestimmt. 
    Der Fehler beträgt hierbei \( \sigma_\vartheta = \SI{0.3}{\degreeCelsius}  + \num{0.005}\vartheta \). 
    Der Fehler des Drucks wird mit \( \SI{0.5}{\bar} \) als halber Skalenteil abgeschätzt.

    \begin{figure}[ht]
        \centering
        \input{images/plot.pgf}
        \caption{Logarithmus des Drucks für die Erwärmungs- und die Abkühlkurve aufgetragen gegen die reziproke Temperatur. Die Messwerte wurden durch die Geraden 
                \( \SI{-5370 (7)}{\K}\cdot x + \SI{25.91 (15)}{} \) für die Erwärmung und \( \SI{-5470 (6)}{\K} \cdot x + \SI{26.11 (12)}{} \) für die Abkühlung gefittet.}
        \label{fig:Plot}
    \end{figure}

    In Abbildung \ref{fig:Plot} ist der Logarithmus des Drucks für die Erwärmung und die Abkühlung gegen die reziproke Temperatur \( T^{-1} \) aufgetragen. 
    Beide Kurven wurden mittels Python durch eine Funktion der Form \( mx+b \) gefittet, die Fitparameter sind in Tabelle \ref{tab:fit} dargestellt.

    \begin{table}[htbp]
        \centering
        \caption{Fitparameter für die Erwärmungs- und Abkühlkurve}
        \begin{tabular}{c|c|c}
            & Erwärmung & Abkühlung\\\hline
            \( m \) & \SI{-5370 (7)}{\K} & \SI{-5470 (6)}{\K}\\
            \( b \) & \SI{25.91 (15)}{} & \SI{26.11 (12)}{}
        \end{tabular}
        \label{tab:fit}
    \end{table}

    Der Dampfdruck \( p \) lässt sich demnach als 
    \begin{align}
        \ln(p) &= mT^{-1} + b\\
        \implies p &= e^{mT^{-1} + b}
        \label{eq:Druck}
    \end{align}
    bestimmen. Gleichsetzen mit Gleichung \eqref{eq: Clausius Clapeyron integriert} führt zu 
    \begin{align}
        p_0 \cdot e^{\frac{\Lambda_V}{R} \left( \frac{1}{T_0} - \frac{1}{T} \right)} &= e^{mT^{-1} + b}\\
        \implies -\frac{\Lambda}{R T} + \frac{\Lambda}{R T_0} + \ln(p_0) &= \frac{m}{T} + b,
    \end{align}
    woraus \( m = -\frac{\Lambda}{R} \) und \( b = + \frac{\Lambda}{R T_0} + \ln(p_0) \) und damit für die Verdampfungswärme \( \Lambda_V \) und die Siedetemperatur \( T_0 \) 
    \begin{align}
        \Lambda_V &= -mR\\
        T_0 &= \frac{\Lambda_V}{\left( b - ln(p_0) \right)R} = \frac{m}{b - ln(p_0)}
        \label{eq:Siedetemp}
    \end{align}
    folgt. 

    Die Verdampfungswärme errechnet sich im Mittelwert der Erwärmungs- und Abkühlkurve so zu \SI{45100 (300)}{\J \per \mol}, wobei sich der Fehler nach gaußscher 
    Fehlerfortpflanzung nach 
    \begin{equation}
        \sigma_{\Lambda_V} = \sigma_m R
        % \label{eq:}
    \end{equation}
    errechnet.

    \subsection{Dampfdruck}

        Bei Normaldruck von \( p_n = \SI{101325}{\Pa} \) \cite[S.189]{Dem1} beträgt der gewichtete Mittelwert der Siedetemperatur nach Gleichung \eqref{eq:Siedetemp} 
        \SI{373 (4)}{\K} mit Fehler 
        \begin{equation}
            \sigma_{T_0} =\sqrt{\left( \frac{\sigma_m}{b-\ln(p_n)} \right)^2 + \frac{\left( \sigma_b m \right)^2}{\left( b-\ln(p_n) \right)^4}}.
            % \label{eq:}
        \end{equation}

        Der Dampfdruck von Wasser bei \( \SI{0}{\degreeCelsius} \hat{=} \SI{273.15}{\K} \) lässt sich nach Gleichung \eqref{eq:Druck} im gewichteten Mittelwert zu 
        \SI{483 (90)}{\Pa} bestimmen, der Fehler wird nach gaußscher Fehlerfortpflanzung als 
        \begin{equation}
            \sigma_p = p\sqrt{\left( \frac{\sigma_m}{T_0} \right)^2 + \sigma_b^2}
            %\label{eq:}
        \end{equation}
        berechnet.

    \subsection{Siedetemperatur auf der Zugspitze}

        Die Zugspitze liegt auf einer Höhe von \SI{2962}{\m} über NN. Die Siedetemperatur auf dieser Höhe lässt sich durch Gleichsetzen von Gleichung 
        \eqref{eq: Clausius Clapeyron integriert} und \eqref{eq:baroHöhe} als 
        \begin{align}
            p_0 e^{-\frac{h}{H}} &= p_0 e^{\frac{\Lambda_V}{R}\left( \frac{1}{T_0} + \frac{1}{T} \right)}\\
            \implies -\frac{h}{H} &= \frac{\Lambda_V}{RT_0} - \frac{\Lambda_V}{RT}\\
            \implies T &= \frac{1}{\frac{1}{T_0} + \frac{hR}{H\Lambda_V}}
        \end{align}
        zu \SI{364.56 (7)}{\K} bestimmen. Hierbei ist \( H = \SI{8.33e3}{\m} \) \cite[S.190]{Dem1} die Skalenhöhe und \( T_0 \) die Siedetemperatur auf Höhe \( h= \SI{0}{\m} \) NN. 
        Der Fehler ist hierbei durch 
        \begin{equation}
            \sigma_T = \sqrt{\left( \frac{\sigma_{\Lambda_V} H h R T_0^2}{\left( H\Lambda_V + hRT_0 \right)^2} \right)^2 
            + \left( \frac{\sigma_{T_0} H\Lambda_V}{\left( H\Lambda_V + hRT_0 \right)^2} \right)^2}
            %\label{eq:}
        \end{equation}
        nach Fehlerfortpflanzung gegeben. Der Referenzwert errechnet sich analog unter Verwendung von \( \Lambda_V = \SI{40590}{\J \per \mol} \) 
        und \( T_0 = \SI{373.2}{\K} \)\cite[S. 299]{Gerthsen} zu \( T = \SI{363.32}{\K} \).






    
